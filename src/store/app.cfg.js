/**
 * App config file
 * this is initial idea
 */
export const cfg = {
  header: {
    logo: { src: 'img/logo.png', label: 'Nexter logo' },
    title: 'Your own home:',
    callout: 'Ultimate personal freedom',
    seenon: {
      title: 'As seen on',
      logos: [
        { src: 'img/logo-bbc.png', label: 'BBC logo' },
        { src: 'img/logo-forbes.png', label: 'Forbes logo' },
        {
          src: 'img/logo-techcrunch.png',
          label: 'Techcrunch logo',
        },
        { src: 'img/logo-bi.png', label: 'BI logo' },
      ],
    },
  },
  features: [
    {
      svgIco: 'icon-global',
      title: "World's best luxury homes",
      text: `
    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tenetur distinctio necessitatibus pariatur voluptatibus.
    `,
    },
    {
      svgIco: 'icon-trophy',
      title: 'Only the best properties',
      text: `
    Voluptatum mollitia quae. Vero ipsum sapiente molestias accusamus rerum sed a eligendi aut quia.
    `,
    },
    {
      svgIco: 'icon-map-pin',
      title: 'All homes at top location',
      text: `
    Tenetur distinctio necessitatibus pariatur voluptatibus quidem consequatur harum.
    `,
    },
    {
      svgIco: 'icon-key',
      title: 'New home in one week',
      text: `
    Vero ipsum sapiente molestias accusamus rerum. Lorem, ipsum dolor sit amet consectetur adipisicing elit.
    `,
    },
    {
      svgIco: 'icon-presentation',
      title: 'Top 1% realtors',
      text: `
    Quidem consequatur harum, voluptatum mollitia quae. Tenetur distinctio necessitatibus pariatur voluptatibus.
    `,
    },
    {
      svgIco: 'icon-lock',
      title: 'Secure payments on nexter',
      text: `
    Pariatur voluptatibus quidem consequatur harum, voluptatum mollitia quae.
    `,
    },
  ],
  story: {
    title: 'Happy Customers',
    statement: '&ldquo;The best decision of our lives&rdquo;',
    text:
      'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tenetur distinctio necessitatibus pariatur voluptatibus. Quidem consequatur harum volupta!',
    cta: {
      label: 'Find your home now!',
      action: 'should send you somewhere',
    },
    pictures: [
      {
        img: 'img/story-1.jpeg',
        label: 'Couple with new home',
      },
      {
        img: 'img/story-2.jpeg',
        label: 'New house',
      },
    ],
  },
  svg: {
    //svg icon file
    //sprite holds all icons
    //you refrence using #icon-name
    iconFile: 'img/sprite.svg',
    icoSearch: 'icon-magnifying-glass',
  },
  homes: [
    {
      title: 'Beautiful Family House',
      img: 'img/house-1.jpeg',
      svgIco: 'icon-heart-full',
      location: {
        svgIco: 'icon-map-pin',
        country: 'USA',
        cssClas: 'home-location',
      },
      rooms: { svgIco: 'icon-profile-male', value: '5 rooms' },
      //m2 is in the markup
      area: { svgIco: 'icon-expand', value: '325' },
      price: { svgIco: 'icon-key', value: '$2.200.000' },
    },
    {
      title: 'Modern glass villa',
      img: 'img/house-2.jpeg',
      svgIco: 'icon-heart-full',
      location: {
        svgIco: 'icon-map-pin',
        country: 'Canada',
        cssClas: 'home-location',
      },
      rooms: { svgIco: 'icon-profile-male', value: '5 rooms' },
      //m2 is in the markup
      area: { svgIco: 'icon-expand', value: '625' },
      price: { svgIco: 'icon-key', value: '$2.750.000' },
    },
    {
      title: 'Cozy Country Home',
      img: 'img/house-3.jpeg',
      svgIco: 'icon-heart-full',
      location: {
        svgIco: 'icon-map-pin',
        country: 'UK',
        cssClas: 'home-location',
      },
      rooms: { svgIco: 'icon-profile-male', value: '4 rooms' },
      //m2 is in the markup
      area: { svgIco: 'icon-expand', value: '525' },
      price: { svgIco: 'icon-key', value: '$550.000' },
    },
    {
      title: 'Large Rustical Villa',
      img: 'img/house-4.jpeg',
      svgIco: 'icon-heart-full',
      location: {
        svgIco: 'icon-map-pin',
        country: 'USA',
        cssClas: 'home-location',
      },
      rooms: { svgIco: 'icon-profile-male', value: '7 rooms' },
      //m2 is in the markup
      area: { svgIco: 'icon-expand', value: '560' },
      price: { svgIco: 'icon-key', value: '$2.200.000' },
    },
    {
      title: 'Majestic Villa',
      img: 'img/house-5.jpeg',
      svgIco: 'icon-heart-full',
      location: {
        svgIco: 'icon-map-pin',
        country: 'Germany',
        cssClas: 'home-location',
      },
      rooms: { svgIco: 'icon-profile-male', value: '18 rooms' },
      //m2 is in the markup
      area: { svgIco: 'icon-expand', value: '1025' },
      price: { svgIco: 'icon-key', value: '$3.500.000' },
    },
    {
      title: 'Modern Familly Apartment',
      img: 'img/house-6.jpeg',
      svgIco: 'icon-heart-full',
      location: {
        svgIco: 'icon-map-pin',
        country: 'Italy',
        cssClas: 'home-location',
      },
      rooms: { svgIco: 'icon-profile-male', value: '4 rooms' },
      //m2 is in the markup
      area: { svgIco: 'icon-expand', value: '125' },
      price: { svgIco: 'icon-key', value: '$666.000' },
    },
  ],
  gallery: {
    houses: [
      { img: 'img/gal-1.jpeg', label: 'house 1' },
      { img: 'img/gal-2.jpeg', label: 'house 2' },
      { img: 'img/gal-3.jpeg', label: 'house 3' },
      { img: 'img/gal-4.jpeg', label: 'house 4' },
      { img: 'img/gal-5.jpeg', label: 'house 5' },
      { img: 'img/gal-6.jpeg', label: 'house 6' },
      { img: 'img/gal-7.jpeg', label: 'house 7' },
      { img: 'img/gal-8.jpeg', label: 'house 8' },
      { img: 'img/gal-9.jpeg', label: 'house 9' },
      { img: 'img/gal-10.jpeg', label: 'house 10' },
      { img: 'img/gal-11.jpeg', label: 'house 11' },
      { img: 'img/gal-12.jpeg', label: 'house 12' },
      { img: 'img/gal-13.jpeg', label: 'house 13' },
      { img: 'img/gal-14.jpeg', label: 'house 14' },
    ],
  },
  footer: {
    links: [
      {
        path: '#dream-home',
        label: 'Find your dream home',
      },
      {
        path: '#proposal',
        label: 'Request proposal',
      },
      {
        path: '#planner',
        label: 'Download home planner',
      },
      {
        path: '#contact',
        label: 'Contact us',
      },
      {
        path: '#submit-property',
        label: 'Submit your property',
      },
      {
        path: '#work',
        label: 'Come work with us',
      },
    ],
    copyright: `
        Copyright 2018 by Jonas Schmedtmann. Feel free to use this project for your own purposes.
        This does NOT apply producing your own course or tutorial based on this project.
      `,
  },
}

export const sidebarPopup = {
  img: 'img/gal-3.jpeg',
  title: 'Nexter - Sidebar component',
  subtitle: 'Clicked on the menu bar',
  body: `
  Hi there! This demo project is product of advanced CSS
  training I followed on Udemy. It is not completely functional
  website. The goal is to learn
  CSS grid layout and demo the knowledge obtained.
  <br/><br/>
  Grid layout defines a two-dimensional grid-based layout system,
  optimized for user interface design. In the grid layout model,
  the children of a grid container can be positioned into arbitrary
  slots in a predefined flexible or fixed-size layout grid.
  <br/><br/>
  Beside CSS grid layout this demo is created using Vue.js.
  The content is split into 12 reusable components.
  `,
}

export const headerPopup = {
  img: 'img/gal-7.jpeg',
  title: 'Nexter - Header component',
  subtitle: 'Clicked on VIEW OUR PROPERTIES!',
  body: `
  <p>
  This demo project is product of advanced CSS
  training I followed on Udemy. It is not completely functional
  website due to scope of the exercise. The goal is to learn
  CSS grid layout and demo CSS knowledge obtained during the training.
  </p><p>
  Grid layout is not supported by older browsers like
  Internet Explorer. According to <a href="https://caniuse.com/#search=grid" target="__new">
  CanIUse statistics</a> of Mei 2019 the grid
  layout is supported by the browsers of 92% of the
  global internet users.
  </p>
  `,
}

export const ctaPopup = {
  img: 'img/gal-1.jpeg',
  title: 'Nexter - Story component',
  subtitle: 'Clicked on Call-To-Action button',
  body: `
  Hi there! This demo project is product of advanced CSS
  training I followed on Udemy. It is not completely functional
  website due to the scope of exercise. The goal was to learn
  CSS grid layout and demo my knowledge of CSS and VueJS.
  <br/><br/>
  CSS is written using SCSS version of SASS. PostCSS
  auto-prefixer is used to automatically add browser prefixes
  based on info from CanIUse website.
  <br/><br/>
  The content is split in 12 reusable Vue components.
  Custom Webpack setup is used to build the project.
  Basic unit tests are performed with Jest. GitLab CI/CD is
  used to automatically test, build and deploy solution
  to GitLab pages.
  `,
}

export const homePopup = {
  img: 'will be assigned in component',
  title: 'Nexter - Home component',
  subtitle: 'Clicked on contact realtor button',
  body: `
    Hi there! This demo project is product of advanced CSS
    training I followed on Udemy. It is not completely functional
    website due to scope of the exercise.
    The content is split into 12 reusable Vue components.
    <br/><br/>
    During the development webpack-dev-server is used for live reloading.
    Basic unit tests are performed with Jest. GitLab CI/CD is
    used to automatically test, build and deploy solution
    to GitLab pages.
  `,
}

export const footerPopup = {
  img: 'img/gal-2.jpeg',
  title: 'Nexter - footer component',
  subtitle: 'Clicked on footer link',
  body: `
  Hi there! This demo project is not completely functional
  website due to the scope of exercise. The goal was to learn
  CSS grid layout and demo my knowledge of CSS and VueJS.
  <br/><br/>
  CSS is written using SCSS version of SASS. PostCSS
  auto-prefixer is used to automatically add browser prefixes
  based on info from CanIUse website.
  <br/><br/>
  The content is split in 12 reusable Vue components.
  Custom Webpack setup is used to build the project.
  Basic unit tests are performed with Jest. GitLab CI/CD is
  used to automatically test, build and deploy solution
  to GitLab pages.
  `,
}
