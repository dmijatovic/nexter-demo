import { shallowMount } from '@vue/test-utils'
import App from './app.vue'

//global component
import appPopup from './system/popupImg.vue'

describe('App.vue', () => {
  it('can perform shallowMount', () => {
    const comp = shallowMount(App, {
      stubs: { 'app-popup': appPopup },
    })
    expect(comp).toBeTruthy()
  })
})
