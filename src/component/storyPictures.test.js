import { shallowMount, createLocalVue } from '@vue/test-utils'
import StoryPictures from './storyPictures.vue'

const localVue = createLocalVue()
const testData = {}

describe('StoryPictures.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(StoryPictures, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
