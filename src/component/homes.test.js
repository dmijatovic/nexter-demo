import { shallowMount, createLocalVue } from '@vue/test-utils'
import Homes from './homes.vue'

const localVue = createLocalVue()
const testData = {
  homes: [],
}

describe('Homes.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Homes, {
      localVue,
      stubs: {
        'home-card': '<div class="home-card">Home card</div>',
      },
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
