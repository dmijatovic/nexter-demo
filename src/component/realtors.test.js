import { shallowMount, createLocalVue } from '@vue/test-utils'
import Realtors from './realtors.vue'

const localVue = createLocalVue()
const testData = {}

describe('Realtors.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Realtors, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
