import { shallowMount, createLocalVue } from '@vue/test-utils'
import Sidebar from './sidebar.vue'

const localVue = createLocalVue()
const testData = {}

describe('Sidebar.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Sidebar, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
