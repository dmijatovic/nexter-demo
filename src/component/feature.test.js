import { shallowMount, createLocalVue } from '@vue/test-utils'
import Feature from './feature.vue'
//import SvgIcon from '../system/svgIcon.vue'

const localVue = createLocalVue()
const testFeature = {
  svgIco: 'test',
  title: 'Test title',
  text: 'Test text',
}

describe('Feature.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Feature, {
      localVue,
      stubs: { 'svg-icon': '<div>SvgIconStub</div>' },
      propsData: {
        feature: testFeature,
      },
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })

  it('shows feature title', () => {
    // find element
    const el = comp.find('.feature-heading')
    //console.log('head...', head())
    expect(el.text()).toEqual(testFeature.title)
  })

  test('shows feature text', () => {
    const el = comp.find('.feature-text')
    //console.log('head...', head())
    expect(el.text()).toEqual(testFeature.text)
  })
})
