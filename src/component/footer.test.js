import { shallowMount, createLocalVue } from '@vue/test-utils'
import Footer from './footer.vue'
//import SvgIcon from '../system/svgIcon.vue'

const localVue = createLocalVue()

const testData = {
  links: [
    {
      path: '#dream-home',
      label: 'Find your dream home',
    },
    {
      path: '#proposal',
      label: 'Request proposal',
    },
  ],
  copyright: 'copyright text!',
}

describe('Footer.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Footer, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })

  it('shows test links', () => {
    comp.setData({ links: testData.links })
    const el = comp.findAll('.nav-link')
    expect(el.length).toBe(2)
  })

  it('shows © and dummy copyright text', () => {
    comp.setData({ copyright: testData.copyright })
    const el = comp.find('.copyright')
    expect(el.text()).toEqual(`© ${testData.copyright}`)
  })
})
