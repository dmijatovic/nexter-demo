import { shallowMount } from '@vue/test-utils'
import Features from './features.vue'

describe('Features.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Features)
  })
  it('can shallowMount', () => {
    expect(comp).toBeTruthy()
  })

  it('contains <feature-item> markup', () => {
    expect(comp.html()).toContain('<feature-item')
  })
})
