import { shallowMount, createLocalVue } from '@vue/test-utils'
import Header from './header.vue'
//import SvgIcon from '../system/svgIcon.vue'

const localVue = createLocalVue()
const testData = {}

describe('Gallery.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Header, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
