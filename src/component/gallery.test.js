import { shallowMount, createLocalVue } from '@vue/test-utils'
import Gallery from './gallery.vue'
//import SvgIcon from '../system/svgIcon.vue'

const localVue = createLocalVue()
const testData = {
  houses: [
    { img: 'img/gal-1.jpeg', label: 'house 1' },
    { img: 'img/gal-2.jpeg', label: 'house 2' },
  ],
}

describe('Gallery.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Gallery, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })

  it('shows test images', () => {
    comp.setData({ houses: testData.houses })
    const el = comp.findAll('.gallery-img')
    expect(el.length).toBe(2)
  })
})
