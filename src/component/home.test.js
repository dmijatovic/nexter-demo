import { shallowMount, createLocalVue } from '@vue/test-utils'
import Home from './home.vue'

const localVue = createLocalVue()
const testData = {
  home: {
    title: 'Home title',
    img: 'home-image.jpg',
    svgIco: 'icon-heart-full',
    location: {
      svgIco: 'icon-map-pin',
      country: 'USA',
      cssClas: 'home-location',
    },
    rooms: { svgIco: 'icon-profile-male', value: '5 rooms' },
    area: { svgIco: 'icon-expand', value: '325' },
    price: { svgIco: 'icon-key', value: '$2.200.000' },
  },
}

describe('Home.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(Home, {
      localVue,
      stubs: { 'svg-icon': '<div>SvgIconStub</div>' },
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
