import { shallowMount, createLocalVue } from '@vue/test-utils'
import StoryContent from './storyContent.vue'

const localVue = createLocalVue()
const testData = {}

describe('StoryContent.vue', () => {
  let comp
  beforeEach(() => {
    comp = shallowMount(StoryContent, {
      localVue,
      propsData: testData,
    })
  })

  it('can shallowMount', () => {
    expect(comp.isVueInstance()).toBeTruthy()
  })
})
